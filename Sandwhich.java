public class Sandwhich{
	
	private String meat;
	private int size;
	private String cheese;
	
	public Sandwhich(String meat, int size, String cheese){
		
		this.meat = meat;
		this.size = size;
		this.cheese = cheese;
	}
	
	public String getMeat(){
		
		return this.meat;
	}
	public int getSize(){
		
		return this.size;
	}
	public String getCheese(){
		
		return this.cheese;
	}
	
	public void setSize(int size){
		
		this.size = size;
	}
	public void setCheese(String cheese){
		
		this.cheese = cheese;
	}
	
	public void eat(){
		
		if(size >= 12){
			
			System.out.println("That was a lot of food!");
		}else{
			
			System.out.println("That was tasty!");
		}
	}
}